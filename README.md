# Content ratings
This repository includes the age-based ratings for apps available on Google Play.

## Rating authorities by region

### North & South America
The [Entertainment Software Rating Board (ESRB)](https://www.esrb.org/) oversees ratings in North and South America.

### Europe & Middle East
Ratings for Europe and the Middle East are managed by [Pan European Game Information (PEGI).](http://www.pegi.info/)

### Germany
In Germany, the [Unterhaltungssoftware Selbstkontrolle (USK)](http://www.usk.de/iarc/) is responsible for ratings.

### Australia (Games only)
For games in Australia, ratings are provided by the [Australian Classification Board (ACB).](http://www.classification.gov.au/) 
Other apps use the "Other countries" category ratings.

### Brazil
The rating system in Brazil is administered by [ClassInd.](http://www.justica.gov.br/seus-direitos/classificacao)

### South Korea
In South Korea, game content ratings are approved by the [GRAC.](http://www.grac.or.kr/english/) 
Non-game apps follow Google Play's own rating system.

### Other countries
For regions not specifically mentioned, ratings are provided by the [International Age Rating Coalition (IARC).](https://www.globalratings.com/ratings-guide.aspx)

## Related resources
For more details, see [Apps & Games content ratings on Google Play.](https://support.google.com/googleplay/answer/6209544?visit_id=638511752323225143-653506638&p=appgame_ratings&rd=1#zippy=%2Ceurope-middle-east%2Cgermany%2Caustralia-games-only%2Cbrazil%2Csouth-korea%2Cother-countries%2Cnorth-south-america)